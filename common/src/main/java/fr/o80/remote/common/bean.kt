package fr.o80.remote.common

data class KeyRequest(val key: String)
data class MouseMoveRequest(val x: Int, val y: Int)

data class JsonResponse(val status: String)