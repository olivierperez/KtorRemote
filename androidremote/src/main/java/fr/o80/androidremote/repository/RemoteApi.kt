package fr.o80.androidremote.repository

import fr.o80.remote.common.KeyRequest
import fr.o80.remote.common.MouseMoveRequest
import io.reactivex.Completable
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * @author Olivier Perez
 */
interface RemoteApi {

    @POST("/secured/move")
    fun moveMouse(@Body body: MouseMoveRequest) : Completable

    @POST("/secured/key/enter")
    fun keyEnter(@Body body: KeyRequest) : Completable

    @POST("/secured/key/press")
    fun keyPress(@Body body: KeyRequest) : Completable

    @POST("/secured/key/release")
    fun keyRelease(@Body body: KeyRequest) : Completable

}