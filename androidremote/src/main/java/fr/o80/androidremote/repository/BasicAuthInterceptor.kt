package fr.o80.androidremote.repository

import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.Response

/**
 * @author Olivier Perez
 */
class BasicAuthInterceptor(user: String, password: String) : Interceptor {

    private val cred = Credentials.basic(user, password)

    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.request()
            .newBuilder()
            .header("Authorization", cred).build()
            .let { chain.proceed(it) }
    }
}