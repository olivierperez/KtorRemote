package fr.o80.androidremote.presentation

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import fr.o80.androidremote.R
import fr.o80.androidremote.service.Memory
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Memory.init(this)

        host.setText(Memory.get("host"))
        user.setText(Memory.get("user"))
        password.setText(Memory.get("password"))

        goBtn.setOnClickListener {
            Memory.login(host.text.toString(), user.text.toString(), password.text.toString())

            startActivity(RemoteActivity.newIntent(this))
            finish()
        }
    }
}
