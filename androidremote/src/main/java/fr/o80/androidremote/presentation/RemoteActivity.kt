package fr.o80.androidremote.presentation

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import fr.o80.androidremote.R
import fr.o80.androidremote.service.KeyboardService
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_remote.*
import java.util.concurrent.TimeUnit

private val TAG = RemoteActivity::class.java.simpleName

class RemoteActivity : AppCompatActivity() {

    private val keyboardService = KeyboardService()
    private var up: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_remote)

        upBtn.setOnTouchListener { _, event ->
            handleEvent(event, keyboardService.pressUp(), keyboardService.releaseUp())
        }
        rightBtn.setOnTouchListener { _, event ->
            handleEvent(event, keyboardService.pressRight(), keyboardService.releaseRight())
        }
        downBtn.setOnTouchListener { _, event ->
            handleEvent(event, keyboardService.pressDown(), keyboardService.releaseDown())
        }
        leftBtn.setOnTouchListener { _, event ->
            handleEvent(event, keyboardService.pressLeft(), keyboardService.releaseLeft())
        }
    }

    private fun handleEvent(
        event: MotionEvent,
        press: Completable,
        release: Completable
                           ): Boolean {
        return when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                Log.d("up", "PRESS")
                up = Observable.interval(0, 300, TimeUnit.MILLISECONDS)
                    .flatMapCompletable { press }
                    .handleSubscription()
                true
            }
            MotionEvent.ACTION_UP   -> {
                Log.d("up", "RELEASE")
                up?.dispose()
                release.handleSubscription()
                true
            }
            else                    -> false
        }
    }

    private fun Completable.handleSubscription() =
        subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { keys.isEnabled = false }
            .subscribeBy(
                onError = { throwable ->
                    Log.e(TAG, "Failed!", throwable)
                    keys.isEnabled = true
                },
                onComplete = {
                    Log.d(TAG, "Success!")
                    keys.isEnabled = true
                })

    companion object {
        fun newIntent(context: Context): Intent = Intent(context, RemoteActivity::class.java)
    }

}
