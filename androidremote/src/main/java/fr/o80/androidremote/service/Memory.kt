package fr.o80.androidremote.service

import android.annotation.SuppressLint
import android.content.Context
import fr.o80.androidremote.repository.BasicAuthInterceptor
import fr.o80.androidremote.repository.RemoteApi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.IllegalStateException
import java.util.concurrent.TimeUnit

/**
 * @author Olivier Perez
 */
@SuppressLint("StaticFieldLeak")
object Memory {

    private var context: Context? = null

    private var _api: RemoteApi? = null

    val api: RemoteApi
        get() = _api
            ?: throw IllegalStateException("Not initialized!")

    fun init(context:Context) {
        this.context = context
    }

    fun login(host: String, user: String, password: String) {
        store("host", host)
        store("user", user)
        store("password", password)

        val httpClient = OkHttpClient.Builder()
            .addInterceptor(BasicAuthInterceptor(user, password))
            .callTimeout(2, TimeUnit.SECONDS)
            .connectTimeout(2, TimeUnit.SECONDS)
            .readTimeout(2, TimeUnit.SECONDS)
            .writeTimeout(2, TimeUnit.SECONDS)
            .build()

        _api = Retrofit.Builder()
            .baseUrl("http://$host")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(httpClient)
            .build()
            .create(RemoteApi::class.java)
    }

    fun get(key: String): String {
        val sharedPreferences = context!!.getSharedPreferences(key, Context.MODE_PRIVATE)
        return sharedPreferences.getString(key, "")!!
    }

    fun store(key: String, value: String) {
        val sharedPreferences = context!!.getSharedPreferences(key, Context.MODE_PRIVATE)
        sharedPreferences.edit()
            .putString(key, value)
            .apply()
    }
}