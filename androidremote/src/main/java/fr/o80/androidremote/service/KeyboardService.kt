package fr.o80.androidremote.service

import fr.o80.androidremote.repository.RemoteApi
import fr.o80.remote.common.KeyRequest
import io.reactivex.Completable

/**
 * @author Olivier Perez
 */
class KeyboardService(private val api: RemoteApi = Memory.api) {

    fun sendUp(): Completable = api.keyEnter(KeyRequest("VK_UP"))
    fun pressUp(): Completable = api.keyPress(KeyRequest("VK_UP"))
    fun releaseUp(): Completable = api.keyRelease(KeyRequest("VK_UP"))

    fun sendRight(): Completable = api.keyEnter(KeyRequest("VK_RIGHT"))
    fun pressRight(): Completable = api.keyPress(KeyRequest("VK_RIGHT"))
    fun releaseRight(): Completable = api.keyRelease(KeyRequest("VK_RIGHT"))

    fun sendDown(): Completable = api.keyEnter(KeyRequest("VK_DOWN"))
    fun pressDown(): Completable = api.keyPress(KeyRequest("VK_DOWN"))
    fun releaseDown(): Completable = api.keyRelease(KeyRequest("VK_DOWN"))

    fun sendLeft(): Completable = api.keyEnter(KeyRequest("VK_LEFT"))
    fun pressLeft(): Completable = api.keyPress(KeyRequest("VK_LEFT"))
    fun releaseLeft(): Completable = api.keyRelease(KeyRequest("VK_LEFT"))

}