# Ktor Remote

Ktor Remote provides simple control of your computer via an HTTP server.

# Routes

```
POST /secured/move
    { "x": 10, "y": 0 }

POST /secured/key/enter
    { "key": "VK_UP"}

POST /secured/key/press
    { "key": "VK_UP"}

POST /secured/key/release
    { "key": "VK_UP"}
```
