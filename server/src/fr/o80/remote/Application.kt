package fr.o80.remote

import fr.o80.remote.common.JsonResponse
import fr.o80.remote.common.KeyRequest
import fr.o80.remote.common.MouseMoveRequest
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.*
import io.ktor.features.*
import io.ktor.gson.gson
import io.ktor.http.CacheControl
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.CachingOptions
import io.ktor.request.path
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.sessions.*
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.date.GMTDate
import org.slf4j.event.Level
import java.awt.MouseInfo
import java.awt.Robot
import java.awt.event.KeyEvent
import java.net.InetAddress
import java.net.NetworkInterface
import kotlin.collections.set

fun main(args: Array<String>) {
    io.ktor.server.netty.EngineMain.main(args)
    showNetworkAddresses()
}

private fun showNetworkAddresses() {
    for (networkInterface in NetworkInterface.getNetworkInterfaces()) {
        println("- ${networkInterface.name} | ${networkInterface.displayName}")

        for (address in networkInterface.inetAddresses) {
            println(address)
        }

        println()
    }
}

@KtorExperimentalAPI
@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    val robot = Robot()

    install()

    routing {
        get("/") {
            call.respondText("<h1>It works!</h1>", contentType = ContentType.Text.Html)
        }

        authenticate("myBasicAuth") {
            route("secured") {
                post("/move") {
                    val request = call.receive<MouseMoveRequest>()
                    try {
                        val pointerInfo = MouseInfo.getPointerInfo()
                        val location = pointerInfo.location
                        robot.mouseMove(
                            location.x + request.x,
                            location.y + request.y
                        )
                        call.respond(JsonResponse("OK"))
                    } catch (e: ContentTransformationException) {
                        call.respond(HttpStatusCode.BadRequest)
                    } catch (e: Exception) {
                        call.respond(HttpStatusCode.InternalServerError)
                    }
                }

                post("/key/enter") {
                    try {
                        val request = call.receive<KeyRequest>()
                        val keyField = KeyEvent::class.java.getDeclaredField(request.key)
                        val keyValue = keyField.getInt(null)
                        robot.keyPress(keyValue)
                        robot.keyRelease(keyValue)
                        call.respond(JsonResponse("OK"))
                    } catch (e: ContentTransformationException) {
                        call.respond(HttpStatusCode.BadRequest)
                    } catch (e:Exception) {
                        call.respond(HttpStatusCode.InternalServerError)
                    }
                }

                post("/key/press") {
                    try {
                        val request = call.receive<KeyRequest>()
                        val keyField = KeyEvent::class.java.getDeclaredField(request.key)
                        val keyValue = keyField.getInt(null)
                        robot.keyPress(keyValue)
                        call.respond(JsonResponse("OK"))
                    } catch (e: ContentTransformationException) {
                        call.respond(HttpStatusCode.BadRequest)
                    } catch (e:Exception) {
                        call.respond(HttpStatusCode.InternalServerError)
                    }
                }

                post("/key/release") {
                    val request = call.receive<KeyRequest>()
                    try {
                        val keyField = KeyEvent::class.java.getDeclaredField(request.key)
                        val keyValue = keyField.getInt(null)
                        robot.keyRelease(keyValue)
                        call.respond(JsonResponse("OK"))
                    } catch (e: ContentTransformationException) {
                        call.respond(HttpStatusCode.BadRequest)
                    } catch (e:Exception) {
                        call.respond(HttpStatusCode.InternalServerError)
                    }
                }
            }
        }
    }
}

private fun Application.install() {
    install(Sessions) {
        cookie<MySession>("MY_SESSION") {
            cookie.extensions["SameSite"] = "lax"
        }
    }

    install(Compression) {
        gzip {
            priority = 1.0
        }
        deflate {
            priority = 10.0
            minimumSize(1024) // condition
        }
    }

    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    install(CachingHeaders) {
        options { outgoingContent ->
            when (outgoingContent.contentType?.withoutParameters()) {
                ContentType.Text.CSS -> CachingOptions(
                    CacheControl.MaxAge(maxAgeSeconds = 24 * 60 * 60),
                    expires = null as? GMTDate?
                                                      )
                else -> null
            }
        }
    }

    install(Authentication) {
        basic("myBasicAuth") {
            realm = "Remote"
            validate { if (it.name == "olivier" && it.password == "apzoeiruty") UserIdPrincipal(it.name) else null }
        }
    }

    install(ContentNegotiation) {
        gson {
        }
    }
}

data class MySession(val count: Int = 0)
